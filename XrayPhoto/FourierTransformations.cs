﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XrayPhoto
{
    internal class FourierTransformations
    {
        private Bitmap originalImage;
        private float lowPassCutoff;
        private float highPassCutoff;
        private Color selectedColor;

        public FourierTransformations(Bitmap image, float lowPassCutoff, float highPassCutoff)
        {
            this.originalImage = image;
            this.lowPassCutoff = lowPassCutoff;
            this.highPassCutoff = highPassCutoff;
        }

        public Bitmap ExtractChannel(Bitmap image, int channelIndex)
        {
            Bitmap channel = new Bitmap(image.Width, image.Height, PixelFormat.Format8bppIndexed);

            BitmapData imageData = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly, image.PixelFormat);
            BitmapData channelData = channel.LockBits(new Rectangle(0, 0, channel.Width, channel.Height), ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

            int bytesPerPixel = Bitmap.GetPixelFormatSize(image.PixelFormat) / 8;
            byte[] imageBytes = new byte[imageData.Stride * image.Height];
            byte[] channelBytes = new byte[channelData.Stride * channel.Height];

            Marshal.Copy(imageData.Scan0, imageBytes, 0, imageBytes.Length);
            image.UnlockBits(imageData);

            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    int imageIndex = y * imageData.Stride + x * bytesPerPixel;
                    channelBytes[y * channelData.Stride + x] = imageBytes[imageIndex + channelIndex];
                }
            }

            Marshal.Copy(channelBytes, 0, channelData.Scan0, channelBytes.Length);
            channel.UnlockBits(channelData);

            return channel;
        }

        public Bitmap ProcessChannel(Bitmap channel, float cutoff, bool isLowPass)
        {
            AForge.Imaging.ComplexImage complexImage = AForge.Imaging.ComplexImage.FromBitmap(channel);
            complexImage.ForwardFourierTransform();

            if (isLowPass)
            {
                ApplyLowPassFilter(complexImage, cutoff);
            }
            else
            {
                ApplyHighPassFilter(complexImage, cutoff);
            }

            complexImage.BackwardFourierTransform();
            return complexImage.ToBitmap();
        }

        public Bitmap CombineChannels(Bitmap red, Bitmap green, Bitmap blue)
        {
            int width = red.Width;
            int height = red.Height;
            Bitmap combinedImage = new Bitmap(width, height, PixelFormat.Format24bppRgb);

            BitmapData redData = red.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);
            BitmapData greenData = green.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);
            BitmapData blueData = blue.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);
            BitmapData combinedData = combinedImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);

            byte[] redBytes = new byte[redData.Stride * height];
            byte[] greenBytes = new byte[greenData.Stride * height];
            byte[] blueBytes = new byte[blueData.Stride * height];
            byte[] combinedBytes = new byte[combinedData.Stride * height];

            Marshal.Copy(redData.Scan0, redBytes, 0, redBytes.Length);
            Marshal.Copy(greenData.Scan0, greenBytes, 0, greenBytes.Length);
            Marshal.Copy(blueData.Scan0, blueBytes, 0, blueBytes.Length);

            red.UnlockBits(redData);
            green.UnlockBits(greenData);
            blue.UnlockBits(blueData);

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    int combinedIndex = y * combinedData.Stride + x * 3;
                    int channelIndex = y * width + x;

                    combinedBytes[combinedIndex] = blueBytes[channelIndex];
                    combinedBytes[combinedIndex + 1] = greenBytes[channelIndex];
                    combinedBytes[combinedIndex + 2] = redBytes[channelIndex];
                }
            }

            Marshal.Copy(combinedBytes, 0, combinedData.Scan0, combinedBytes.Length);
            combinedImage.UnlockBits(combinedData);

            return combinedImage;
        }

        public static int NextPowerOf2(int n)
        {
            int p = 1;
            while (p < n)
            {
                p *= 2;
            }
            return p;
        }

        public void ApplyLowPassFilter(AForge.Imaging.ComplexImage complexImage, float cutoff)
        {
            int width = complexImage.Width;
            int height = complexImage.Height;
            int centerX = width / 2;
            int centerY = height / 2;
            float radius = Math.Min(centerX, centerY) * cutoff;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    double dx = x - centerX;
                    double dy = y - centerY;
                    double distance = Math.Sqrt(dx * dx + dy * dy);

                    if (distance > radius)
                    {
                        complexImage.Data[x, y] = new AForge.Math.Complex(0, 0);
                    }
                }
            }
        }

        public void ApplyHighPassFilter(AForge.Imaging.ComplexImage complexImage, float cutoff)
        {
            int width = complexImage.Width;
            int height = complexImage.Height;
            int centerX = width / 2;
            int centerY = height / 2;
            float radius = Math.Min(centerX, centerY) * cutoff;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    double dx = x - centerX;
                    double dy = y - centerY;
                    double distance = Math.Sqrt(dx * dx + dy * dy);

                    if (distance <= radius)
                    {
                        complexImage.Data[x, y] = new AForge.Math.Complex(0, 0);
                    }
                }
            }
        }

        public void DisplayTransformedImage(Bitmap image, string title)
        {
            Form transformForm = new Form
            {
                Text = title,
                Size = new Size(400, 400)
            };
            PictureBox transformPictureBox = new PictureBox
            {
                Image = image,
                Dock = DockStyle.Fill,
                SizeMode = PictureBoxSizeMode.Zoom
            };
            transformForm.Controls.Add(transformPictureBox);
            transformForm.ShowDialog();
        }
    }

}
