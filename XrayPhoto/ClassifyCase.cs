﻿using System;
using System.Drawing;

namespace XrayPhoto
{
    public class ClassifyCase
    {
        public static string ClassifyImage(Bitmap image)
        {
            // Convert the image to grayscale
            Bitmap grayImage = ConvertToGrayscale(image);

            // Apply histogram analysis
            double meanIntensity = GetMeanIntensity(grayImage);

            // Apply edge detection
            Bitmap edgesImage = ApplyEdgeDetection(grayImage);

            // Analyze edge density
            double edgeDensity = GetEdgeDensity(edgesImage);

            // Use a combination of mean intensity and edge density for classification
            return ClassifyBasedOnFeatures(meanIntensity, edgeDensity);
        }

        private static Bitmap ConvertToGrayscale(Bitmap original)
        {
            Bitmap grayscale = new Bitmap(original.Width, original.Height);
            for (int y = 0; y < original.Height; y++)
            {
                for (int x = 0; x < original.Width; x++)
                {
                    Color originalColor = original.GetPixel(x, y);
                    int grayValue = (int)((originalColor.R * 0.3) + (originalColor.G * 0.59) + (originalColor.B * 0.11));
                    Color grayColor = Color.FromArgb(grayValue, grayValue, grayValue);
                    grayscale.SetPixel(x, y, grayColor);
                }
            }
            return grayscale;
        }

        private static double GetMeanIntensity(Bitmap image)
        {
            long sumIntensity = 0;
            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    Color pixel = image.GetPixel(x, y);
                    sumIntensity += pixel.R;
                }
            }
            return (double)sumIntensity / (image.Width * image.Height);
        }

        private static Bitmap ApplyEdgeDetection(Bitmap image)
        {
            Bitmap edgeImage = new Bitmap(image.Width, image.Height);

            // Simple Sobel filter for edge detection
            int[,] gx = new int[,] { { -1, 0, 1 }, { -2, 0, 2 }, { -1, 0, 1 } };
            int[,] gy = new int[,] { { -1, -2, -1 }, { 0, 0, 0 }, { 1, 2, 1 } };

            for (int y = 1; y < image.Height - 1; y++)
            {
                for (int x = 1; x < image.Width - 1; x++)
                {
                    int edgeX = 0;
                    int edgeY = 0;

                    for (int i = -1; i <= 1; i++)
                    {
                        for (int j = -1; j <= 1; j++)
                        {
                            Color neighbor = image.GetPixel(x + j, y + i);
                            edgeX += neighbor.R * gx[i + 1, j + 1];
                            edgeY += neighbor.R * gy[i + 1, j + 1];
                        }
                    }

                    int edgeMagnitude = (int)Math.Sqrt((edgeX * edgeX) + (edgeY * edgeY));
                    edgeMagnitude = Math.Min(255, Math.Max(0, edgeMagnitude));
                    edgeImage.SetPixel(x, y, Color.FromArgb(edgeMagnitude, edgeMagnitude, edgeMagnitude));
                }
            }
            return edgeImage;
        }

        private static double GetEdgeDensity(Bitmap edgeImage)
        {
            int edgeCount = 0;
            for (int y = 0; y < edgeImage.Height; y++)
            {
                for (int x = 0; x < edgeImage.Width; x++)
                {
                    Color pixel = edgeImage.GetPixel(x, y);
                    if (pixel.R > 128) // Consider it an edge if intensity is above a threshold
                    {
                        edgeCount++;
                    }
                }
            }
            return (double)edgeCount / (edgeImage.Width * edgeImage.Height);
        }

        private static string ClassifyBasedOnFeatures(double meanIntensity, double edgeDensity)
        {
            // Define thresholds for classification based on analysis of mean intensity and edge density
            if (meanIntensity < 85 && edgeDensity > 0.15)
                return "Severe";
            else if (meanIntensity < 170 && edgeDensity > 0.10)
                return "Moderate";
            else
                return "Light";
        }
    }
}
