﻿//using NAudio.Wave;
//using System;
//using System.IO;

//namespace XrayPhoto
//{
//    public class AudioRecord
//    {
//        private WaveInEvent waveSource;
//        private WaveFileWriter waveFile;
//        private string audioFilePath;

//        public string AudioFilePath => audioFilePath;

//        public void StartRecording(string recordingsDirectory)
//        {
//            if (waveSource == null)
//            {
//                waveSource = new WaveInEvent();
//                waveSource.WaveFormat = new WaveFormat(44100, 1);

//                waveSource.DataAvailable += WaveSource_DataAvailable;
//                waveSource.RecordingStopped += WaveSource_RecordingStopped;

//                audioFilePath = Path.Combine(recordingsDirectory, "recordedAudio.wav");

//                waveFile = new WaveFileWriter(audioFilePath, waveSource.WaveFormat);

//                waveSource.StartRecording();
//            }
//        }

//        public void StopRecording()
//        {
//            if (waveSource != null)
//            {
//                waveSource.StopRecording();
//            }
//        }

//        private void WaveSource_DataAvailable(object sender, WaveInEventArgs e)
//        {
//            if (waveFile != null)
//            {
//                waveFile.Write(e.Buffer, 0, e.BytesRecorded);
//                waveFile.Flush();
//            }
//        }

//        private void WaveSource_RecordingStopped(object sender, StoppedEventArgs e)
//        {
//            if (waveFile != null)
//            {
//                waveFile.Dispose();
//                waveFile = null;
//            }

//            if (waveSource != null)
//            {
//                waveSource.Dispose();
//                waveSource = null;
//            }
//        }

//        public void PlayRecording()
//        {
//            if (File.Exists(audioFilePath))
//            {
//                using (var audioFile = new AudioFileReader(audioFilePath))
//                using (var outputDevice = new WaveOutEvent())
//                {
//                    outputDevice.Init(audioFile);
//                    outputDevice.Play();
//                }
//            }
//            else
//            {
//                throw new FileNotFoundException("No recording found. Please record first.");
//            }
//        }
//    }
//}
