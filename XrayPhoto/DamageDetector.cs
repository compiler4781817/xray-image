﻿//using OpenCvSharp;
//using System;
//using System.Drawing;

//namespace XrayPhoto
//{
//    internal class DamageDetector
//    {
//        public static Rectangle[] DetectDamagedAreas(Bitmap image, Rectangle selectionRect)
//        {
//            using (Mat mat = OpenCvSharp.Extensions.BitmapConverter.ToMat(image))
//            using (Mat gray = new Mat())
//            using (Mat edges = new Mat())
//            {
//                // Convert image to grayscale
//                Cv2.CvtColor(mat, gray, ColorConversionCodes.BGR2GRAY);

//                // Apply Canny edge detection
//                Cv2.Canny(gray, edges, 100, 200);

//                // Find contours
//                Cv2.FindContours(edges, out var contours, out _, RetrievalModes.External, ContourApproximationModes.ApproxSimple);

//                // Initialize a mask for the selection rectangle
//                Mat selectionMask = Mat.Zeros(mat.Size(), MatType.CV_8U);

//                // Draw the selection rectangle on the mask
//                selectionMask.Rectangle(new OpenCvSharp.Point(selectionRect.X, selectionRect.Y), new OpenCvSharp.Point(selectionRect.Right, selectionRect.Bottom), Scalar.White, -1);

//                // Initialize a mask for the detected contours
//                Mat contourMask = Mat.Zeros(mat.Size(), MatType.CV_8U);

//                // Draw the contours on the contour mask
//                Cv2.DrawContours(contourMask, contours, -1, Scalar.White, thickness: -1);

//                // Apply bitwise AND operation between the contour mask and the selection mask
//                Mat maskedContours = new Mat();
//                Cv2.BitwiseAnd(contourMask, selectionMask, maskedContours);

//                // Find contours in the masked image
//                Cv2.FindContours(maskedContours, out var maskedContoursList, out _, RetrievalModes.External, ContourApproximationModes.ApproxSimple);

//                // Determine bounding rectangles for contours
//                var boundingRectangles = new Rectangle[maskedContoursList.Length];
//                for (int i = 0; i < maskedContoursList.Length; i++)
//                {
//                    // Get the bounding rectangle for the contour
//                    Rect rect = Cv2.BoundingRect(maskedContoursList[i]);

//                    // Convert OpenCvSharp.Rect to System.Drawing.Rectangle
//                    boundingRectangles[i] = new Rectangle(rect.X, rect.Y, rect.Width, rect.Height);
//                }

//                return boundingRectangles;
//            }
//        }


//    }
//}
