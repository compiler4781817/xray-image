﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XrayPhoto
{
    internal class SelectedAreaColor
    {
        private Bitmap originalImage;
        private Color selectedColor;

        public SelectedAreaColor(Bitmap image, Color color)
        {
            this.originalImage = image;
            this.selectedColor = color;
        }

        public void CreateColoredOverlayWithinRectangle(Rectangle selectionRect)
        {
            selectionRect.Intersect(new Rectangle(0, 0, originalImage.Width, originalImage.Height));

            for (int y = selectionRect.Top; y < selectionRect.Bottom; y++)
            {
                for (int x = selectionRect.Left; x < selectionRect.Right; x++)
                {
                    int pixelX = x;
                    int pixelY = y;

                    if (pixelX >= 0 && pixelX < originalImage.Width && pixelY >= 0 && pixelY < originalImage.Height)
                    {
                        Color originalColor = originalImage.GetPixel(pixelX, pixelY);
                        int intensity = (originalColor.R + originalColor.G + originalColor.B) / 3;
                        double severity = intensity / 255.0;

                        Color color = GetColorForSeverity(severity);
                        originalImage.SetPixel(pixelX, pixelY, color);
                    }
                }
            }
        }

        public void CreateColoredOverlayWithinEllipse(Rectangle selectionRect)
        {
            selectionRect.Intersect(new Rectangle(0, 0, originalImage.Width, originalImage.Height));

            int centerX = selectionRect.Left + selectionRect.Width / 2;
            int centerY = selectionRect.Top + selectionRect.Height / 2;
            int radiusX = selectionRect.Width / 2;
            int radiusY = selectionRect.Height / 2;

            for (int y = selectionRect.Top; y < selectionRect.Bottom; y++)
            {
                for (int x = selectionRect.Left; x < selectionRect.Right; x++)
                {
                    double normalizedX = (x - centerX) / (double)radiusX;
                    double normalizedY = (y - centerY) / (double)radiusY;

                    if (normalizedX * normalizedX + normalizedY * normalizedY <= 1)
                    {
                        int pixelX = x;
                        int pixelY = y;

                        if (pixelX >= 0 && pixelX < originalImage.Width && pixelY >= 0 && pixelY < originalImage.Height)
                        {
                            Color originalColor = originalImage.GetPixel(pixelX, pixelY);
                            int intensity = (originalColor.R + originalColor.G + originalColor.B) / 3;
                            double severity = intensity / 255.0;

                            Color color = GetColorForSeverity(severity);
                            originalImage.SetPixel(pixelX, pixelY, color);
                        }
                    }
                }
            }
        }

        public void CreateColoredOverlayWithinFreehand(List<Point> freehandPoints)
        {
            Rectangle imageBounds = new Rectangle(0, 0, originalImage.Width, originalImage.Height);

            using (GraphicsPath path = new GraphicsPath())
            {
                path.AddLines(freehandPoints.ToArray());
                path.CloseFigure();

                using (Region region = new Region(path))
                {
                    for (int y = 0; y < originalImage.Height; y++)
                    {
                        for (int x = 0; x < originalImage.Width; x++)
                        {
                            if (region.IsVisible(x, y))
                            {
                                Color originalColor = originalImage.GetPixel(x, y);
                                int intensity = (originalColor.R + originalColor.G + originalColor.B) / 3;
                                double severity = intensity / 255.0;

                                Color color = GetColorForSeverity(severity);
                                originalImage.SetPixel(x, y, color);
                            }
                        }
                    }
                }
            }
        }

        public void CreateColoredOverlayWithinPolygon(List<Point> polygonPoints)
        {
            using (GraphicsPath path = new GraphicsPath())
            {
                path.AddLines(polygonPoints.ToArray());
                path.CloseFigure();

                using (Region region = new Region(path))
                {
                    for (int y = 0; y < originalImage.Height; y++)
                    {
                        for (int x = 0; x < originalImage.Width; x++)
                        {
                            if (region.IsVisible(x, y))
                            {
                                Color originalColor = originalImage.GetPixel(x, y);
                                int intensity = (originalColor.R + originalColor.G + originalColor.B) / 3;
                                double severity = intensity / 255.0;

                                Color color = GetColorForSeverity(severity);
                                originalImage.SetPixel(x, y, color);
                            }
                        }
                    }
                }
            }
        }

        private Color GetColorForSeverity(double severity)
        {
            double blendFactor = 1 - severity;
            int r = (int)(selectedColor.R * (1 - blendFactor) + 255 * blendFactor);
            int g = (int)(selectedColor.G * (1 - blendFactor) + 255 * blendFactor);
            int b = (int)(selectedColor.B * (1 - blendFactor) + 255 * blendFactor);

            return Color.FromArgb(r, g, b);
        }
    }


}
