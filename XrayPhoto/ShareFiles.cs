﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot;
using System.IO;

namespace XrayPhoto
{
    internal class ShareFiles
    {
        public List<string> userChatIds = new List<string>();
        public string chatIdsFilePath = "UserChatIds.txt"; // Update with the actual path

        public async Task ShareViaTelegram(string[] files, string botToken)
        {
            if (string.IsNullOrEmpty(botToken))
            {
                MessageBox.Show("Bot token is not set.");
                return;
            }

            var botClient = new TelegramBotClient(botToken);
            await GetAndSaveChatIds(botClient); // Get and save chat IDs

            foreach (var chatId in userChatIds)
            {
                foreach (var file in files)
                {
                    using (var fileStream = File.OpenRead(file))
                    {
                        try
                        {
                            var inputFile = new InputOnlineFile(fileStream, Path.GetFileName(file));
                            await botClient.SendDocumentAsync(chatId, inputFile);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show($"Failed to send file {file} to chat ID {chatId}: {ex.Message}");
                        }
                    }
                }
            }
        }

        public async Task GetAndSaveChatIds(TelegramBotClient botClient)
        {
            try
            {
                var updates = await botClient.GetUpdatesAsync();

                // Log the number of updates received
                MessageBox.Show($"Number of updates received: {updates.Length}");

                foreach (var update in updates)
                {
                    if (update.Message != null)
                    {
                        string chatId = update.Message.Chat.Id.ToString();
                        SaveUserChatId(chatId); // Save new chat IDs

                        MessageBox.Show($"Received message from chat ID: {chatId}, chat type: {update.Message.Chat.Type}");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error getting chat IDs: {ex.Message}");
            }
        }

        public void SaveUserChatId(string chatId)
        {
            if (!userChatIds.Contains(chatId))
            {
                userChatIds.Add(chatId);
                File.AppendAllText(chatIdsFilePath, chatId + Environment.NewLine);
            }
        }

        public void ShareViaWhatsApp(string[] files)
        {
            string message = "Here are the files: " + string.Join(", ", files);
            string whatsappUrl = $"https://api.whatsapp.com/send?text={Uri.EscapeDataString(message)}";
            try
            {
                Process.Start(new ProcessStartInfo(whatsappUrl) { UseShellExecute = true });
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed to open WhatsApp: {ex.Message}");
            }
        }
    }

}
