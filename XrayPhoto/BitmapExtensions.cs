﻿//using Emgu.CV;
//using Emgu.CV.Structure;
//using System.Drawing;

//namespace XrayPhoto
//{
//    public static class BitmapExtensions
//    {
//        public static Image<Bgr, byte> ToImage(this Bitmap bitmap)
//        {
//            // Convert the Bitmap to an Image<Bgr, byte>
//            return new Image<Bgr, byte>(bitmap.Width, bitmap.Height);
//        }

//        // Convert Image<Bgr, byte> to Bitmap
//        public static Bitmap ToBitmap(this Image<Bgr, byte> image)
//        {
//            // Create a new Bitmap with the same dimensions as the image
//            Bitmap bitmap = new Bitmap(image.Width, image.Height);

//            // Copy pixel data from the image to the bitmap
//            for (int y = 0; y < image.Height; y++)
//            {
//                for (int x = 0; x < image.Width; x++)
//                {
//                    Bgr pixelColor = image[y, x];
//                    Color color = Color.FromArgb((int)pixelColor.Red, (int)pixelColor.Green, (int)pixelColor.Blue);
//                    bitmap.SetPixel(x, y, color);
//                }
//            }

//            return bitmap;
//        }
//    }
//}
