﻿using System.Drawing;
using System.Windows.Forms;
using System;
using System.Collections.Generic;

public class HighlightManager
{
    private PictureBox pictureBox;
    private Image originalImage; // Reference to the original image

    public Rectangle SelectionRect { get; private set; }
    private bool isSelecting;
    private Point startPoint;
    public string SelectedShape { get; set; } = "Rectangle"; // Default shape

    private List<Point> freehandPoints = new List<Point>();
    private bool isDrawingFreehand = false;
    private List<Point> polygonPoints = new List<Point>();
    private bool isDrawingPolygon = false;

    public HighlightManager(PictureBox pictureBox)
    {
        this.pictureBox = pictureBox;
        this.pictureBox.Paint += PictureBox_Paint;
        this.pictureBox.MouseDown += PictureBox_MouseDown;
        this.pictureBox.MouseMove += PictureBox_MouseMove;
        this.pictureBox.MouseUp += PictureBox_MouseUp;
    }

    public void SetOriginalImage(Image image)
    {
        originalImage = image;
        pictureBox.Invalidate(); // Ensure the picture box is redrawn with the new image
    }

    public void PictureBox_MouseDown(object sender, MouseEventArgs e)
    {
        if (e.Button == MouseButtons.Left)
        {
            if (SelectedShape == "Freehand")
            {
                isDrawingFreehand = true;
                freehandPoints.Clear();
                freehandPoints.Add(e.Location);
                pictureBox.Invalidate();
            }
            else if (SelectedShape == "Polygon")
            {
                isDrawingPolygon = true;
                polygonPoints.Add(e.Location);
                pictureBox.Invalidate();
            }
            else
            {
                isSelecting = true;
                startPoint = e.Location;
                SelectionRect = new Rectangle(startPoint, new Size(0, 0)); // Initialize selection rectangle
                pictureBox.Invalidate();
            }
        }
    }

    public void PictureBox_MouseMove(object sender, MouseEventArgs e)
    {
        if (isSelecting)
        {
            int x = Math.Min(e.X, startPoint.X);
            int y = Math.Min(e.Y, startPoint.Y);
            int width = Math.Abs(e.X - startPoint.X);
            int height = Math.Abs(e.Y - startPoint.Y);
            SelectionRect = new Rectangle(x, y, width, height);
            pictureBox.Invalidate();
        }
        else if (isDrawingFreehand)
        {
            freehandPoints.Add(e.Location);
            pictureBox.Invalidate();
        }
    }

    public void PictureBox_MouseUp(object sender, MouseEventArgs e)
    {
        if (e.Button == MouseButtons.Left)
        {
            if (isSelecting)
            {
                isSelecting = false;
                int x = Math.Min(e.X, startPoint.X);
                int y = Math.Min(e.Y, startPoint.Y);
                int width = Math.Abs(e.X - startPoint.X);
                int height = Math.Abs(e.Y - startPoint.Y);
                SelectionRect = new Rectangle(x, y, width, height);
                pictureBox.Invalidate();
            }
            else if (isDrawingFreehand)
            {
                isDrawingFreehand = false;
                freehandPoints.Add(e.Location);
                pictureBox.Invalidate();
            }
        }
    }

    public List<Point> GetFreehandPoints()
    {
        return new List<Point>(freehandPoints);
    }
    public List<Point> GetPolygonPoints()
    {
        return new List<Point>(polygonPoints);
    }
    public void DrawSelectionRect(Graphics g)
    {
        if (originalImage != null)
        {
            if (SelectedShape == "Freehand" && freehandPoints.Count > 1)
            {
                // Draw freehand shape
                using (Pen pen = new Pen(Color.Red, 2))
                {
                    g.DrawLines(pen, freehandPoints.ToArray());
                }
            }
            else if (SelectedShape == "Polygon" && polygonPoints.Count > 1)
            {
                // Draw polygon shape
                using (Pen pen = new Pen(Color.Red, 2))
                {
                    g.DrawPolygon(pen, polygonPoints.ToArray());
                }
            }
            else if (!SelectionRect.IsEmpty)
            {
                // Convert selection rectangle coordinates from PictureBox to image coordinates
                RectangleF rect = GetImageRectangle(SelectionRect, originalImage.Size, pictureBox.ClientSize);

                using (Pen pen = new Pen(Color.Red, 2))
                {
                    switch (SelectedShape)
                    {
                        case "Rectangle":
                            g.DrawRectangle(pen, rect.X, rect.Y, rect.Width, rect.Height);
                            break;
                        case "Ellipse":
                            g.DrawEllipse(pen, rect.X, rect.Y, rect.Width, rect.Height);
                            break;
                    }
                }
            }
        }
    }

    private RectangleF GetImageRectangle(Rectangle rect, Size imageSize, Size controlSize)
    {
        // Calculate the scaling factors for X and Y dimensions
        float scaleX = (float)imageSize.Width / controlSize.Width;
        float scaleY = (float)imageSize.Height / controlSize.Height;

        // Convert the coordinates of the selection rectangle from PictureBox to image coordinates
        float x = rect.X * scaleX;
        float y = rect.Y * scaleY;
        float width = rect.Width * scaleX;
        float height = rect.Height * scaleY;

        // Return the transformed rectangle
        return new RectangleF(x, y, width, height);
    }

    private void PictureBox_Paint(object sender, PaintEventArgs e)
    {
        // Draw the original image
        if (originalImage != null)
        {
            e.Graphics.DrawImage(originalImage, new Rectangle(0, 0, pictureBox.Width, pictureBox.Height));

            // Draw the selection rectangle or freehand drawing
            DrawSelectionRect(e.Graphics);
        }
    }
}
