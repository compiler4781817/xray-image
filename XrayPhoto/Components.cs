﻿using Emgu.CV.Structure;
using Emgu.CV;
using NAudio.Wave;
using Org.BouncyCastle.Asn1.Crmf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XrayPhoto
{
    internal class Components
    {
        //private Button openFileButton;
        //private Button openFileButtonB;
        //private Button saveFileButton;
        //private Button compareButton;
        //private Button classifyButton;
        //private TextBox textCommentTextBox;
        //private Button searchButton;
        //private WaveIn waveIn;
        //private float lowPassCutoff = 0.3f;
        //private float highPassCutoff = 0.3f;
        //private WaveFileWriter writer;
        //private string outputFile;
        //private PictureBox xrayPictureBox;
        //private GroupBox searchGroupBox;
        //private GroupBox reportGroupBox;
        //private GroupBox fourierGroupBox;
        //private PictureBox CropedPictureBox;
        //private PictureBox xrayPictureBoxB;
        //private ColorDialog colorDialog;
        //private Color selectedColor;
        //private Button selectColorButton;
        //private Button colorDamagedButton;
        //private Button applyFourierTransformButton;
        //private Button cropImageButton;
        //private Button drawShapesButton;
        //private ImageHandler imageHandler;
        //private HighlightManager highlightManager;
        //private TextBox sizeTextBox;
        //private TextBox dateTextBox;
        //private WaveInEvent waveSource;
        //private WaveFileWriter waveFile;
        //private string audioFilePath;
        //private string recordingsDirectory;
        //private string reportsDirectory;
        private ShareFiles shareFiles;
        private Form1 form;
        //private static List<string> userChatIds = new List<string>();

        //  private const string chatIdsFilePath = "UserChatIds.txt"; 
        // private string audioFilePath;
        private Image<Bgr, byte> currentImage;

        public Components()
        {
            form = new Form1();
        }


        public static void InitializeAdditionalComponents( Form1 form)
        {
            form.BackColor = Color.MediumTurquoise;
            // TextBox for report
            TextBox reportTextBox = new TextBox
            {
                Location = new Point(1200, 40),
                Size = new Size(300, 150),
                Multiline = true
            };

            // Button to save report
            Button saveReportButton = new Button
            {
                Text = "Save Report",
                Location = new Point(1200, 210),
                Size = new Size(100, 30),
                BackColor = Color.LightSkyBlue,
                FlatStyle = FlatStyle.Flat,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            saveReportButton.Click += (sender, e) => form.SaveReport(reportTextBox.Text);


            // Button to export report as PDF
            Button exportPdfButton = new Button
            {
                Text = "Export as PDF",
                Location = new Point(1200, 260),
                Size = new Size(100, 30),
                BackColor = Color.LightSkyBlue,
                FlatStyle = FlatStyle.Flat,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            exportPdfButton.Click += (sender, e) => form.ExportReportAsPdf(reportTextBox.Text);


            // Button to compress files
            Button compressFilesButton = new Button
            {
                Text = "Compress Files",
                Location = new Point(1200, 310),
                Size = new Size(100, 30),
                BackColor = Color.LightSkyBlue,
                FlatStyle = FlatStyle.Flat,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            compressFilesButton.Click +=form.CompressFilesButton_Click;


            // Button to share files
            Button shareFilesButton = new Button
            {
                Text = "Share Files",
                Location = new Point(1200, 360),
                Size = new Size(100, 30),
                BackColor = Color.LightSkyBlue,
                FlatStyle = FlatStyle.Flat,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            shareFilesButton.Click += form.ShareFilesButton_Click;

            form.reportGroupBox = new GroupBox
            {
                Text = "Add Report",

                Location = new Point(1200, 10),
                Size = new Size(300, 400), // Adjust size as needed
            };




            Label lowPassLabel = new Label
            {
                Text = "Low Pass",
                Location = new Point(830, 300),
                AutoSize = true
            };

            // Create and configure Low Pass Slider
            TrackBar lowPassSlider = new TrackBar
            {
                Minimum = 1,
                Maximum = 30,
                Value = (int)(form.lowPassCutoff * 100),
                Location = new Point(880, 300),
                TickStyle = TickStyle.None,
                Tag = "LowPass"
            };
            lowPassSlider.Scroll += form.FilterSlider_Scroll;

            // Create and configure High Pass Label
            Label highPassLabel = new Label
            {
                Text = "High Pass",
                Location = new Point(830, 250),
                AutoSize = true
            };

            // Create and configure High Pass Slider
            TrackBar highPassSlider = new TrackBar
            {
                Minimum = 1,
                Maximum = 30,
                Value = (int)(form.highPassCutoff * 100),
                Location = new Point(880, 250),
                TickStyle = TickStyle.None,
                Tag = "HighPass"
            };
            highPassSlider.Scroll += form.FilterSlider_Scroll;
          form.fourierGroupBox = new GroupBox
            {
                Text = "Fourier Transformations",

                Location = new Point(820, 200),
                Size = new Size(250, 200), // Adjust size as needed
            };


            form.Controls.Add(lowPassLabel);
            form.Controls.Add(lowPassSlider);
            form.Controls.Add(highPassLabel);
            form.Controls.Add(highPassSlider);
            form.Controls.Add(form.fourierGroupBox);
            form.Controls.Add(reportTextBox);
            form.Controls.Add(shareFilesButton);
            form.Controls.Add(saveReportButton);
            form.Controls.Add(compressFilesButton);
            form.Controls.Add(exportPdfButton);


            form.Controls.Add(form.reportGroupBox);




        }
        public static void InitializeCustomComponents1(Form1 form)
        {
            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
          form.  recordingsDirectory = Path.Combine(desktopPath, "XrayPhotoRecordings");
            Directory.CreateDirectory(form.recordingsDirectory);
            form.reportsDirectory = Path.Combine(desktopPath, "reports");
            Directory.CreateDirectory(form.reportsDirectory);

            // Initialize the PictureBox for the first image
            form.xrayPictureBox = new PictureBox
            {
                Location = new Point(10, 80),
                Size = new Size(400, 400),
                BorderStyle = BorderStyle.FixedSingle,
                SizeMode = PictureBoxSizeMode.Zoom,
            };

            // Initialize the PictureBox for the second image
            form.xrayPictureBoxB = new PictureBox
            {
                Location = new Point(420, 80),
                Size = new Size(400, 400),
                BorderStyle = BorderStyle.FixedSingle,
                SizeMode = PictureBoxSizeMode.Zoom,
            };

            form.CropedPictureBox = new PictureBox
            {
                Location = new Point(500, 490),
                Size = new Size(300, 300),
                BorderStyle = BorderStyle.FixedSingle,
                SizeMode = PictureBoxSizeMode.Zoom,
            };

            form.xrayPictureBox.Paint += form.PictureBox_Paint;

            // Initialize HighlightManager and attach event handlers
            form.highlightManager = new HighlightManager(form.xrayPictureBox);
            form.xrayPictureBox.MouseDown += form.highlightManager.PictureBox_MouseDown;
            form.xrayPictureBox.MouseMove += form.highlightManager.PictureBox_MouseMove;
            form.xrayPictureBox.MouseUp += form.highlightManager.PictureBox_MouseUp;

            // Color Damaged button
            form.colorDamagedButton = new Button
            {
                Text = "Color Damaged",
                Location = new Point(300, 10),
                Size = new Size(120, 30),
                FlatStyle = FlatStyle.Flat,
                BackColor = Color.LightSkyBlue,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft

            };
            form.colorDamagedButton.Click += form.ColorDamagedButton_Click;

            // Label
            Label processingLabel = new Label
            {
                Text = "Xray Processing",
                Font = new Font("Arial", 44, FontStyle.Bold),
                ForeColor = Color.MediumPurple,
                BackColor = Color.Transparent,
                AutoSize = true,
                Location = new Point(form.ClientSize.Width - 320, form.ClientSize.Height - 20),
                Anchor = AnchorStyles.Bottom | AnchorStyles.Right,
                TextAlign = ContentAlignment.MiddleCenter
            };

            form.Controls.Add(processingLabel);
            form.Resize += (s, e) =>
            {
                processingLabel.Location = new Point(form.ClientSize.Width - processingLabel.Width - 20, form.ClientSize.Height - processingLabel.Height - 20);
            };



            // Open File button for first image
            form.openFileButton = new Button
            {
                Text = "Open X-ray Photo A",
                Location = new Point(10, 10),
                Size = new Size(120, 30),
                FlatStyle = FlatStyle.Flat,
                BackColor = Color.LightSkyBlue,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            form.openFileButton.Click += form.OpenFileButton_Click; // Attach event handler

            // Open File button for second image
            form.openFileButtonB = new Button
            {
                Text = "Open X-ray Photo B",
                Location = new Point(140, 10),
                Size = new Size(120, 30),
                BackColor = Color.LightSkyBlue,
                FlatStyle = FlatStyle.Flat,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            form.openFileButtonB.Click += form.OpenFileButtonB_Click; // Attach event handler

            // Save File button
            form.saveFileButton = new Button
            {
                Text = "Save Image",
                Location = new Point(10, 50),
                Size = new Size(120, 30),
                BackColor = Color.LightSkyBlue,
                FlatStyle = FlatStyle.Flat,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            form.saveFileButton.Click += form.SaveFileButton_Click; // Attach event handler

            form.selectColorButton = new Button
            {
                Text = "Select Color",
                Location = new Point(460, 50),
                Size = new Size(120, 30),
                FlatStyle = FlatStyle.Flat,
                BackColor = Color.LightSkyBlue,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            form.selectColorButton.Click += form.SelectColorButton_Click;

            form.compareButton = new Button
            {
                Text = "Compare X-rays",
                Location = new Point(460, 10),
                Size = new Size(120, 30),
                BackColor = Color.LightSkyBlue,
                FlatStyle = FlatStyle.Flat,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            form.compareButton.Click += form.CompareButton_Click;

            form.classifyButton = new Button
            {
                Text = "Classify X-ray",
                Location = new Point(590, 10),
                Size = new Size(120, 30),
                FlatStyle = FlatStyle.Flat,
                BackColor = Color.LightSkyBlue,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            form.classifyButton.Click += form.ClassifyButton_Click;

            form.sizeTextBox = new TextBox
            {
                Text = "Enter Size in Bytes",
                ForeColor = Color.Gray,
                Location = new Point(940, 50),
                Size = new Size(150, 30),

                Cursor = Cursors.Hand,

            }; 
            form.sizeTextBox.Enter += form.RemovePlaceholderText;
            form.sizeTextBox.Leave += form.AddPlaceholderText;

            // Initialize date textbox
            form.dateTextBox = new TextBox
            {
                Text = "Enter Date (yyyy-MM-dd)",
                ForeColor = Color.Gray,
                Location = new Point(940, 90),
                Size = new Size(150, 30),
                Cursor = Cursors.Hand,
            };
            form.dateTextBox.Enter += form.RemovePlaceholderText;
            form.dateTextBox.Leave += form.AddPlaceholderText;

            form.searchButton = new Button
            {
                Text = "Search",
                Location = new Point(940, 10),
                Size = new Size(80, 30),
                FlatStyle = FlatStyle.Flat,
                BackColor = Color.LightSkyBlue,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            form.searchButton.Click += form.SearchButton_Click;

            // Initialize Fourier Transform button
            form.applyFourierTransformButton = new Button
            {
                Text = "Apply Fourier Transform",
                Location = new Point(830, 360),
                Size = new Size(150, 30),
                BackColor = Color.LightSkyBlue,
                FlatStyle = FlatStyle.Flat,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            form.applyFourierTransformButton.Click += form.ApplyFourierTransformButton_Click;

            // Initialize Crop Image button
            form.cropImageButton = new Button
            {
                Text = "Crop Image",
                Location = new Point(300, 50),
                Size = new Size(120, 30),
                BackColor = Color.LightSkyBlue,
                FlatStyle = FlatStyle.Flat,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            form.cropImageButton.Click += form.CropImageButton_Click;



            // Add TextBox for text comment
            form.textCommentTextBox = new TextBox
            {
                Location = new Point(140, 550),
                Size = new Size(300, 30), // Adjust size as needed
            };

            // Add button to add comment
            Button addCommentButton = new Button
            {
                Text = "Add Comment",
                Location = new Point(10, 550),
                Size = new Size(120, 30),
                BackColor = Color.LightSkyBlue,
                FlatStyle = FlatStyle.Flat,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            addCommentButton.Click += form.AddCommentButton_Click;

            // Add a button for recording
            Button recordButton = new Button
            {
                Text = "Record Voiceover",
                Location = new Point(10, 652),
                Size = new Size(120, 30),
                BackColor = Color.LightSkyBlue,
                FlatStyle = FlatStyle.Flat,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            recordButton.Click += form.RecordButton_Click;

            Button playButton = new Button
            {
                Text = "Play",
                Location = new Point(150, 652), // Adjust location as needed
                Size = new Size(100, 30), // Adjust size as needed
                BackColor = Color.LightSkyBlue,
                FlatStyle = FlatStyle.Flat,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            playButton.Click += form.PlayButton_Click;

            // Add the Open Directory button
            Button openDirectoryButton = new Button
            {
                Text = "Open Recording Directory",
                Location = new Point(10, 710), // Adjust location as needed
                Size = new Size(200, 30), // Adjust size as needed
                BackColor = Color.LightSkyBlue,
                FlatStyle = FlatStyle.Flat,
                Cursor = Cursors.Hand,
                ImageAlign = ContentAlignment.MiddleLeft
            };
            openDirectoryButton.Click += form.OpenDirectoryButton_Click;

            ComboBox shapeComboBox = new ComboBox
            {

                Location = new Point(140, 50),
                BackColor = Color.LightSkyBlue,
                Size = new Size(120, 30),
                DropDownStyle = ComboBoxStyle.DropDownList
            };
            shapeComboBox.Items.AddRange(new string[] { "Rectangle", "Ellipse", "Freehand", "Polygon" });
            //   shapeComboBox.SelectedIndex = 0; // Default to Rectangle

            shapeComboBox.SelectedIndexChanged += form.ShapeComboBox_SelectedIndexChanged;

           form. colorDialog = new ColorDialog();

            // Add components to the form
            form.Controls.Add(shapeComboBox);
            form.Controls.Add( form.colorDamagedButton);
            form.Controls.Add(form.xrayPictureBox);
            form.Controls.Add(form.xrayPictureBoxB);
            form.Controls.Add(form.openFileButton);
            form.Controls.Add(form.openFileButtonB);
            form.Controls.Add(form.saveFileButton);
            form.Controls.Add(form.selectColorButton);
            form.Controls.Add(form.compareButton);
            form.Controls.Add(form.classifyButton);
            form.Controls.Add(form.CropedPictureBox);
            form.Controls.Add(form.applyFourierTransformButton);
            form.Controls.Add(form.cropImageButton);

            form.Controls.Add(form.sizeTextBox);
            form.Controls.Add(form.dateTextBox);
            form.Controls.Add(form.searchButton);
            form.Controls.Add(addCommentButton);
            form.Controls.Add(recordButton);
            form.Controls.Add(playButton);
            form.Controls.Add(openDirectoryButton);
            form.Controls.Add(form.textCommentTextBox);
            form.Controls.Add(form.fourierGroupBox);
        }




    }
}
