﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using AForge.Imaging;
using AForge.Imaging.Filters;
using Accord.Imaging;
using Accord.Statistics;
using NAudio.Wave;
using AForge.Math;
using XrayPhoto;
using static System.Net.Mime.MediaTypeNames;
using System.Runtime.InteropServices;

using System.Diagnostics;
using System.IO.Compression;
using Org.BouncyCastle.Asn1.Crmf;
using Telegram.Bot;
using System.Threading.Tasks;
using Telegram.Bot.Types.InputFiles;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
namespace XrayPhoto
{
   public partial class Form1 : Form
    {

        public Button openFileButton;
        public Button openFileButtonB;
        public Button saveFileButton;
        public Button compareButton;
        public Button classifyButton;
        public TextBox textCommentTextBox;
        public Button searchButton;
        public WaveIn waveIn;
        public float lowPassCutoff = 0.3f;
        public float highPassCutoff = 0.3f;
        public WaveFileWriter writer;
        public string outputFile;
        public PictureBox xrayPictureBox;
        public GroupBox searchGroupBox;
        public GroupBox reportGroupBox;
        public GroupBox fourierGroupBox;
        public PictureBox CropedPictureBox;
        public PictureBox xrayPictureBoxB;
        public ColorDialog colorDialog;
        public Color selectedColor;
        public Button selectColorButton;
        public Button colorDamagedButton;
        public Button applyFourierTransformButton;
        public Button cropImageButton;
        public Button drawShapesButton;
        public ImageHandler imageHandler;
        public HighlightManager highlightManager;
        public TextBox sizeTextBox;
        public TextBox dateTextBox;
        public WaveInEvent waveSource;
        public WaveFileWriter waveFile;
        public string audioFilePath;
        public string recordingsDirectory;
        public string reportsDirectory;
        private ShareFiles shareFiles;
        public static List<string> userChatIds = new List<string>();
        public Image<Bgr, byte> currentImage;

        public Form1()
        {
 
            Components.InitializeCustomComponents1(this);
            Components.InitializeAdditionalComponents(this);
            shareFiles = new ShareFiles();

        }
        public void Form1_Load(object sender, EventArgs e)
        {

        }
  
        public void ShapeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (highlightManager != null)
            {
                highlightManager.SelectedShape = ((ComboBox)sender).SelectedItem.ToString();
            }
        }
        public void SaveReport(string reportContent)
        {
            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.InitialDirectory = reportsDirectory;
                saveFileDialog.Filter = "Text Files (*.txt)|*.txt";
                saveFileDialog.FileName = $"MedicalReport_{DateTime.Now:yyyyMMddHHmmss}.txt";

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string reportPath = saveFileDialog.FileName;

                    try
                    {
                        using (var stream = new FileStream(reportPath, FileMode.Create, FileAccess.Write, FileShare.None))
                        using (var writer = new StreamWriter(stream))
                        {
                            writer.Write(reportContent);
                        }
                        MessageBox.Show($"Report saved at {reportPath}");
                    }
                    catch (IOException ex)
                    {
                        MessageBox.Show($"An error occurred while saving the report: {ex.Message}");
                    }
                }
            }
        }
        public void ExportReportAsPdf(string reportContent)
        {
            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.InitialDirectory = reportsDirectory;
                saveFileDialog.Filter = "PDF Files (*.pdf)|*.pdf";
                saveFileDialog.FileName = $"MedicalReport_{DateTime.Now:yyyyMMddHHmmss}.pdf";

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string pdfPath = saveFileDialog.FileName;

                    try
                    {
                        using (var stream = new FileStream(pdfPath, FileMode.Create, FileAccess.Write, FileShare.None))
                        using (var document = new iTextSharp.text.Document())
                        {
                            var writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, stream);
                            document.Open();
                            document.Add(new iTextSharp.text.Paragraph(reportContent));
                            document.Close();
                        }

                        MessageBox.Show($"PDF exported at {pdfPath}");
                    }
                    catch (IOException ex)
                    {
                        MessageBox.Show($"An error occurred while creating the PDF: {ex.Message}");
                    }
                }
            }
        }
        public void CompressFilesButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Multiselect = true;
                openFileDialog.InitialDirectory = reportsDirectory;
                openFileDialog.Filter = "All Files (*.*)|*.*";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string[] filesToCompress = openFileDialog.FileNames;
                    string zipPath = Path.Combine(reportsDirectory, $"CompressedFiles_{DateTime.Now:yyyyMMddHHmmss}.zip");

                    try
                    {
                        using (var zip = ZipFile.Open(zipPath, ZipArchiveMode.Create))
                        {
                            foreach (var file in filesToCompress)
                            {
                                try
                                {
                                    using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.None))
                                    {
                                        // Ensure the file is closed before adding to zip
                                    }
                                    zip.CreateEntryFromFile(file, Path.GetFileName(file));
                                }
                                catch (IOException ioEx)
                                {
                                    MessageBox.Show($"File {file} could not be added to the zip: {ioEx.Message}");
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Error compressing files: {ex.Message}");
                    }
                }
            }
        }
        public async void ShareFilesButton_Click(object sender, EventArgs e)
        {
            // Open file dialog to select files to share
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = recordingsDirectory,
                Multiselect = true
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string[] selectedFiles = openFileDialog.FileNames;
                Clipboard.SetText(string.Join(Environment.NewLine, selectedFiles));
                MessageBox.Show("File paths copied to clipboard. You can now share them via social media or messaging apps.");

                // Ask the user how they want to share
                DialogResult result = MessageBox.Show("Do you want to share via Telegram or WhatsApp?", "Share Files", MessageBoxButtons.YesNoCancel);

                string botToken = "7003673007:AAE1FOyJOtrEVKLatQUyPzqSt9ruj2SL9pw"; // Replace with your actual bot token

                if (result == DialogResult.Yes)
                {
                    await shareFiles.ShareViaTelegram(selectedFiles, botToken);
                }
                else if (result == DialogResult.No)
                {
                    shareFiles.ShareViaWhatsApp(selectedFiles);
                }
            }
        }
        public void RecordButton_Click(object sender, EventArgs e)
        {
            if (waveSource == null)
            {
                waveSource = new WaveInEvent();
                waveSource.WaveFormat = new WaveFormat(44100, 1);

                waveSource.DataAvailable += WaveSource_DataAvailable;
                waveSource.RecordingStopped += WaveSource_RecordingStopped;

                audioFilePath = Path.Combine(recordingsDirectory, "recordedAudio.wav");

                waveFile = new WaveFileWriter(audioFilePath, waveSource.WaveFormat);

                waveSource.StartRecording();
                MessageBox.Show("Recording started. Click OK to stop recording.");
                waveSource.StopRecording();
            }
            else
            {
                waveSource.StopRecording();
                MessageBox.Show("Recording stopped.");
            }
        }
        public void WaveSource_DataAvailable(object sender, WaveInEventArgs e)
        {
            if (waveFile != null)
            {
                waveFile.Write(e.Buffer, 0, e.BytesRecorded);
                waveFile.Flush();
            }
        }
        public void WaveSource_RecordingStopped(object sender, StoppedEventArgs e)
        {
            if (waveFile != null)
            {
                waveFile.Dispose();
                waveFile = null;
            }

            if (waveSource != null)
            {
                waveSource.Dispose();
                waveSource = null;
            }
        }
        public void PlayButton_Click(object sender, EventArgs e)
        {
            if (File.Exists(audioFilePath))
            {
                using (var audioFile = new AudioFileReader(audioFilePath))
                using (var outputDevice = new WaveOutEvent())
                {
                    outputDevice.Init(audioFile);
                    outputDevice.Play();
                    MessageBox.Show("Playing recording.");
                }
            }
            else
            {
                MessageBox.Show("No recording found. Please record first.");
            }
        }
        public void OpenDirectoryButton_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(recordingsDirectory))
            {
                Process.Start("explorer.exe", recordingsDirectory);
            }
            else
            {
                MessageBox.Show("Recording directory not found.");
            }
        }
        public void AddCommentButton_Click(object sender, EventArgs e)
        {
            if (xrayPictureBox.Image == null)
            {
                MessageBox.Show("Please load an image first.");
                return;
            }

            // Get the text from the TextBox
            string commentText = textCommentTextBox.Text.Trim();

            // Check if the comment text is empty
            if (string.IsNullOrWhiteSpace(commentText))
            {
                MessageBox.Show("Please enter a comment.");
                return;
            }

            using (Graphics g = Graphics.FromImage(xrayPictureBox.Image))
            {
                // Define font and brush for the text
                Font font = new Font(FontFamily.GenericSansSerif, 30);
                SolidBrush brush = new SolidBrush(Color.Blue);

                // Draw the text on the image
                g.DrawString(commentText, font, brush, new PointF(10, 10)); // Adjust position as needed
            }

            xrayPictureBox.Refresh(); // Refresh PictureBox to display the modified image
        }
        public void RemovePlaceholderText(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox != null && textBox.ForeColor == Color.Gray)
            {
                textBox.Text = "";
                textBox.ForeColor = Color.Black;
            }
        }
        public void AddPlaceholderText(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox != null && string.IsNullOrWhiteSpace(textBox.Text))
            {
                if (textBox == sizeTextBox)
                {
                    textBox.Text = "Enter Size in Bytes";
                }
                else if (textBox == dateTextBox)
                {
                    textBox.Text = "Enter Date (yyyy-MM-dd)";
                }
                textBox.ForeColor = Color.Gray;
            }
        }
        public void PictureBox_Paint(object sender, PaintEventArgs e)
        {
            highlightManager.DrawSelectionRect(e.Graphics);
        }
        public void ColorDamagedButton_Click(object sender, EventArgs e)
        {
            if (highlightManager == null ||
                (highlightManager.SelectedShape != "Freehand" && highlightManager.SelectedShape != "Polygon" && highlightManager.SelectionRect.IsEmpty))
            {
                MessageBox.Show("Please select an area first.");
                return;
            }

            Bitmap loadedImage = xrayPictureBox.Image as Bitmap;
            if (loadedImage == null)
            {
                MessageBox.Show("Please load an image first.");
                return;
            }

            SelectedAreaColor selectedAreaColor = new SelectedAreaColor(loadedImage, selectedColor);

            if (highlightManager.SelectedShape == "Freehand")
            {
                selectedAreaColor.CreateColoredOverlayWithinFreehand(highlightManager.GetFreehandPoints());
            }
            else if (highlightManager.SelectedShape == "Polygon")
            {
                selectedAreaColor.CreateColoredOverlayWithinPolygon(highlightManager.GetPolygonPoints());
            }
            else
            {
                Rectangle selectionRect = highlightManager.SelectionRect;

                if (selectionRect.Width <= 0 || selectionRect.Height <= 0)
                {
                    MessageBox.Show("Selected area is invalid. Please select a valid area.");
                    return;
                }

                if (highlightManager.SelectedShape == "Ellipse")
                {
                    selectedAreaColor.CreateColoredOverlayWithinEllipse(selectionRect);
                }
                else
                {
                    selectedAreaColor.CreateColoredOverlayWithinRectangle(selectionRect);
                }
            }

            xrayPictureBox.Refresh();
        }
        public void SelectColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                selectedColor = colorDialog.Color;
            }
        }
        public void OpenFileButton_Click(object sender, EventArgs e)
        {
            // Open image file and set it to the PictureBox
            imageHandler = new ImageHandler(); // Initialize ImageHandler if it's null
            System.Drawing.Image image = imageHandler.OpenImage(xrayPictureBox);

            if (image != null)
            {
                // Pass the original image to the HighlightManager
                highlightManager.SetOriginalImage(image);
            }
        }
        public void OpenFileButtonB_Click(object sender, EventArgs e)
        {
            // Open second image file and set it to the PictureBoxB
            imageHandler = new ImageHandler(); // Initialize ImageHandler if it's null
            System.Drawing.Image image = imageHandler.OpenImage(xrayPictureBoxB);

            if (image != null)
            {
                // You can handle additional logic for the second image if needed
            }
        }
        public void SaveFileButton_Click(object sender, EventArgs e)
        {
            if (imageHandler == null)
                imageHandler = new ImageHandler(); // Initialize if needed
            imageHandler.SaveImage(xrayPictureBox, highlightManager.SelectionRect);
        }
        public void CompareButton_Click(object sender, EventArgs e)
        {
            if (xrayPictureBox.Image == null || xrayPictureBoxB.Image == null)
            {
                MessageBox.Show("Please load both X-ray images first.");
                return;
            }

            Bitmap imageA = new Bitmap(xrayPictureBox.Image);
            Bitmap imageB = new Bitmap(xrayPictureBoxB.Image);

            bool progress = CompareImages(imageA, imageB);

            if (progress)
            {
                //  resultLabel.Text = "Result: Progress in treatment or disease evolution.";

                // resultLabel.Text = $"Classification: {classification}";
                string resultMessage = string.Join(Environment.NewLine, "Result: Progress in treatment or disease evolution.");
                MessageBox.Show(resultMessage, "classification Result");
            }
            else
            {
                // resultLabel.Text = "Result: No progress in treatment or disease evolution.";
                string resultMessage = string.Join(Environment.NewLine, "Result:No Progress in treatment or disease evolution.");
                MessageBox.Show(resultMessage, "classification Result");
            }
        }
        public bool CompareImages(Bitmap imageA, Bitmap imageB)
        {
            // Basic comparison logic (can be enhanced with more sophisticated algorithms)
            int width = Math.Min(imageA.Width, imageB.Width);
            int height = Math.Min(imageA.Height, imageB.Height);

            int diffCount = 0;
            int totalPixels = width * height;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Color pixelA = imageA.GetPixel(x, y);
                    Color pixelB = imageB.GetPixel(x, y);

                    // Calculate difference in intensity
                    int intensityA = (pixelA.R + pixelA.G + pixelA.B) / 3;
                    int intensityB = (pixelB.R + pixelB.G + pixelB.B) / 3;

                    if (Math.Abs(intensityA - intensityB) > 20) // Threshold can be adjusted
                    {
                        diffCount++;
                    }
                }
            }

            double diffPercentage = (double)diffCount / totalPixels;

            // Return true if differences are significant (adjust threshold as needed)
            return diffPercentage > 0.05;
        }
        public void ClassifyButton_Click(object sender, EventArgs e)
        {
            if (xrayPictureBox.Image == null)
            {
                MessageBox.Show("Please load an X-ray image first.");
                return;
            }

            string classification = ClassifyCase.ClassifyImage(xrayPictureBox.Image as Bitmap);
            string resultMessage = $"Classification: {classification}";
            MessageBox.Show(resultMessage, "Classification Result");
        }
        public void SearchButton_Click(object sender, EventArgs e)
        {
            // Implement search logic based on size and modification date
            string sizeText = sizeTextBox.Text == "Enter Size in Bytes" ? "" : sizeTextBox.Text;
            string dateText = dateTextBox.Text == "Enter Date (yyyy-MM-dd)" ? "" : dateTextBox.Text;

            var results = SearchImages(sizeText, dateText);
            //resultLabel.Text = "Search Results:\n" + string.Join("\n", results);
            // Display search results (for simplicity, showing in a message box)
            string resultMessage = string.Join(Environment.NewLine, results);
            MessageBox.Show(resultMessage, "Search Results");
        }
        public string[] SearchImages(string sizeText, string dateText)
        {
            // Dummy implementation of search (replace with actual logic to search stored images)
            string[] allImages = Directory.GetFiles("C:\\Users\\ali\\Desktop\\", "*.jpg");

            var results = allImages.Where(img =>
            {
                FileInfo fileInfo = new FileInfo(img);
                bool sizeMatch = string.IsNullOrEmpty(sizeText) || fileInfo.Length.ToString() == sizeText;
                bool dateMatch = string.IsNullOrEmpty(dateText) || fileInfo.LastWriteTime.ToString("yyyy-MM-dd") == dateText;

                return sizeMatch && dateMatch;
            }).ToArray();

            return results;
        }
        public void CropImageButton_Click(object sender, EventArgs e)
        {
            if (highlightManager == null || highlightManager.SelectionRect.IsEmpty)
            {
                MessageBox.Show("Please select an area first.");
                return;
            }

            Rectangle selectionRect = highlightManager.SelectionRect;
            if (selectionRect.Width <= 0 || selectionRect.Height <= 0)
            {
                MessageBox.Show("Selected area is invalid. Please select a valid area.");
                return;
            }

            Bitmap originalImage = xrayPictureBox.Image as Bitmap;
            if (originalImage == null)
            {
                MessageBox.Show("Please load an image first.");
                return;
            }

            Rectangle cropRect = new Rectangle(selectionRect.Left * originalImage.Width / xrayPictureBox.Width,
                                               selectionRect.Top * originalImage.Height / xrayPictureBox.Height,
                                               selectionRect.Width * originalImage.Width / xrayPictureBox.Width,
                                               selectionRect.Height * originalImage.Height / xrayPictureBox.Height);

            Bitmap croppedImage = new Bitmap(cropRect.Width, cropRect.Height);
            using (Graphics g = Graphics.FromImage(croppedImage))
            {
                g.DrawImage(originalImage, new Rectangle(0, 0, croppedImage.Width, croppedImage.Height), cropRect, GraphicsUnit.Pixel);
            }

            CropedPictureBox.Image = croppedImage;
        }
        public void FilterSlider_Scroll(object sender, EventArgs e)
        {
            if (sender is TrackBar slider)
            {
                if (slider.Tag.ToString() == "LowPass")
                {
                    lowPassCutoff = slider.Value / 100f;
                }
                else if (slider.Tag.ToString() == "HighPass")
                {
                    highPassCutoff = slider.Value / 100f;
                }

                //  ApplyFourierTransform();
            }
        }
        public void ApplyFourierTransformButton_Click(object sender, EventArgs e)
        {
            if (xrayPictureBox.Image == null)
            {
                MessageBox.Show("Please load an X-ray photo first.");
                return;
            }

            // Retrieve the original image from the PictureBox
            Bitmap originalImage = xrayPictureBox.Image as Bitmap;
            if (originalImage == null)
            {
                MessageBox.Show("Error in retrieving the loaded image. Please ensure the image is valid.");
                return;
            }

            // Ensure image dimensions are powers of 2
            int newWidth = FourierTransformations.NextPowerOf2(originalImage.Width);
            int newHeight = FourierTransformations.NextPowerOf2(originalImage.Height);

            if (originalImage.Width != newWidth || originalImage.Height != newHeight)
            {
                // Resize the image to dimensions that are powers of 2
                ResizeBilinear resizeFilter = new ResizeBilinear(newWidth, newHeight);
                originalImage = resizeFilter.Apply(originalImage);
            }

            FourierTransformations ft = new FourierTransformations(originalImage, lowPassCutoff, highPassCutoff);

            // Split image into RGB channels
            Bitmap redChannel = ft.ExtractChannel(originalImage, 2);   // Red
            Bitmap greenChannel = ft.ExtractChannel(originalImage, 1); // Green
            Bitmap blueChannel = ft.ExtractChannel(originalImage, 0);  // Blue

            // Process each channel with low-pass and high-pass filters
            Bitmap processedRedLowPass = ft.ProcessChannel(redChannel, lowPassCutoff, true);
            Bitmap processedGreenLowPass = ft.ProcessChannel(greenChannel, lowPassCutoff, true);
            Bitmap processedBlueLowPass = ft.ProcessChannel(blueChannel, lowPassCutoff, true);

            Bitmap processedRedHighPass = ft.ProcessChannel(redChannel, highPassCutoff, false);
            Bitmap processedGreenHighPass = ft.ProcessChannel(greenChannel, highPassCutoff, false);
            Bitmap processedBlueHighPass = ft.ProcessChannel(blueChannel, highPassCutoff, false);

            // Combine channels back into color images
            Bitmap resultImageLowPass = ft.CombineChannels(processedRedLowPass, processedGreenLowPass, processedBlueLowPass);
            Bitmap resultImageHighPass = ft.CombineChannels(processedRedHighPass, processedGreenHighPass, processedBlueHighPass);

            // Display the transformed images in new forms
            ft.DisplayTransformedImage(resultImageLowPass, "Low-Pass Filtered Image");
            ft.DisplayTransformedImage(resultImageHighPass, "High-Pass Filtered Image");

            //xrayPictureBox.Image = resultImageHighPass;
            // xrayPictureBox.Refresh();
        }



    }
}


